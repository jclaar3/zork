#include "stdafx.h"
#include "funcs.h"
#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix/stl.hpp>

#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;
namespace qi = boost::spirit::qi;
namespace phoenix = boost::phoenix;
namespace ascii = boost::spirit::ascii;

template <typename Iterator>
bool parse_psetg(const char* header, Iterator& first, Iterator last, string& var_name, string& var_value)
{
    using qi::lexeme;
    using qi::lit;
    using ascii::char_;
    using ascii::space;
    using qi::phrase_parse;

    string s1, s2;
    static const qi::rule<std::string::const_iterator, std::string()> qs_rule
        = '"' >> *('\\' >> qi::char_ | ~qi::char_('"')) >> '"';
    //auto qs_rule = '"' >> *~char_('"') >> '"';
    bool r = phrase_parse(first, last,
        (
            lexeme[header >> +(char_ - '\"' - ',' - '#' - '<') >> qs_rule >> ">"]), space, s1, s2
    );
    if (r)
    {
        var_name = s1;
        var_value = s2;
        // Remove any spaces, tabs, and linefeeds from var_name.
        auto p = var_name.find_last_not_of("\n\t ");
        if (p != var_name.size())
        {
            var_name.erase(p + 1, string::npos);
        }
    }
    return r;
}

template <typename Iterator>
bool parse_psetg(const char* header, Iterator& first, Iterator last, string& name, vector<string>& v)
{
    v.clear();
    using qi::lexeme;
    using qi::lit;
    using qi::_1;
    using ascii::char_;
    using ascii::space;
    using qi::phrase_parse;
    using phoenix::push_back;

    static const qi::rule<std::string::const_iterator, std::string()> qs_rule
        = '"' >> *('\\' >> qi::char_ | ~qi::char_('"')) >> '"';
    string vname;
    bool r = phrase_parse(first, last,
        (
            lexeme[header >> +(char_ - '\"' - ',' - '#' - '\'') >> "'["]), space, vname
    );
    if (r)
    {
        name = vname;
        r = phrase_parse(first, last,
            qs_rule >> *(qs_rule), space, v);
    }
    return r;
}

size_t dump_string_array(size_t str, const string &mdl, std::ostream &os)
{
    size_t count = 0;
    bool first = true;
    // Assumes that str points to the first string.
    while (mdl[str] != ']')
    {
        if (!first)
            os << ", ";
        first = false;
        str = dump_string(str, mdl, os);
        str = passws(str, mdl);
        ++count;
    }
    return count;
}

string convert_to_c_style(const string& v)
{
    string rv;
    // Convert to lower-case.
    transform(v.begin(), v.end(), back_inserter(rv), [](char c)
        {
            c = tolower(c);
            if (c == '-')
                c = '_';
            return c;
        });
    return rv;
}

void proc_strings(const string &str, bool header_only, ostream &os)
{
    if (!header_only)
        throw std::runtime_error("String generation only supported for header files.");
    os << "#pragma once\n";
    os << "#include <array>\n";
    os << "#include <string>\n";
    os << "#include <string_view>\n";
    os << "using namespace std::string_view_literals;\n";

    const char* hdrs[] = { "<PSETG", "<SETG" };
    for (auto header : hdrs)
    {
        auto pos = str.find(header);
        string var_name, value;
        vector<string> arr;
        while (pos != string::npos)
        {
            auto cur_iter = str.begin() + pos;
            if (parse_psetg(header, cur_iter, str.end(), var_name, value))
            {
                var_name = convert_to_c_style(var_name);
                cout << "constexpr std::string_view ";
                cout << var_name << " = R\"~(" << value << ")~\";" << endl;
            }
            else if (parse_psetg(header, cur_iter, str.end(), var_name, arr))
            {
                var_name = convert_to_c_style(var_name);
                cout << "constexpr auto";
                cout << var_name << " = std::to_array({ ";
                for (size_t i = 0; i < arr.size(); ++i)
                {
                    cout << "R\"~(" << arr[i] << ")~\"sv";
                    if (i < arr.size() - 1)
                        cout << ", ";
                }
                cout << "});\n";
            }
            pos = str.find(header, pos + 1);
        }
    }
}
