@echo off
echo #pragma once
echo // This file is auto-generated. Do not edit.
echo #include ^"defs.h^"
echo namespace room_funcs {
awk -F"room_funcs::" "{if (length($2) > 0) print $2}" %1 | awk -F")" "{printf \"    RAPPLIC(%%s);\n\", substr($1,1, length($1)-1)}" > tmp.txt
sort tmp.txt > tmp2.txt
awk "!seen[$0]++" tmp2.txt > tmp.txt
type tmp.txt
echo }

echo namespace exit_funcs {
awk -F"exit_funcs::" "{if (length($2) > 0) print $2}" %1 | awk -F")" "{printf \"    EX_RAPPLIC(%%s);\n\", substr($1,1, length($1)-1)}" > tmp.txt
sort tmp.txt > tmp2.txt
echo     EX_RAPPLIC(bkleavee); >> tmp2.txt
echo     EX_RAPPLIC(carousel_out); >> tmp2.txt
echo     EX_RAPPLIC(magnet_room_exit); >> tmp2.txt
echo     EX_RAPPLIC(mrgo); >> tmp2.txt
echo     EX_RAPPLIC(mirin); >> tmp2.txt
echo     EX_RAPPLIC(mirout); >> tmp2.txt
echo     EX_RAPPLIC(maybe_door); >> tmp2.txt
awk "!seen[$0]++" tmp2.txt > tmp.txt
type tmp.txt
echo }
del tmp2.txt
del tmp.txt
