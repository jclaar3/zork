@echo off
echo #pragma once
echo // This file is auto-generated. Do not edit.
echo #include ^"defs.h^"
echo namespace obj_funcs {

awk -F"obj_funcs::" ^
    "{if (length($2) > 0) print $2}" %1 |^
awk -F")" "{ if ($1 ~ /balloon/ || $1 ~ /barrel/ || $1 ~ /rboat_function/ || $1 ~ /bucket/) printf \"    RAPPLIC_RARG\"; else printf(\"    RAPPLIC\"); printf \"(%%s);\n\", substr($1,1,length($1)-1)}" ^
    > tmp.txt

awk -F"obj_funcs::" ^
    "{if (length($2) > 0) print $2}" %2 |^
awk -F")" "{ printf(\"    RAPPLIC(%%s);\n\", substr($1, 1, length($1)-1))}" >> tmp.txt

sort tmp.txt > tmp2.txt
awk "!seen[$0]++" tmp2.txt
echo }

del tmp2.txt
del tmp.txt
