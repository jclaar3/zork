#!/bin/bash
# This script generates the room function prototype list for both rooms (in the room_funcs namespace)
# and exits (in the exit_funcs namespace). It's a first try at using awk and various utilities, and
# there is almost certainly a more efficient way to do this. However, this does replace the func_dump
# utility used previously.

echo "#pragma once"
echo // This file is auto-generated. Do not edit.
echo "#include \"defs.h\""
echo namespace room_funcs {
awk -F'room_funcs::' '{if (length($2) > 0) print $2}' $1 | awk -F')' '{printf "    RAPPLIC(%s);\n",substr($1, 1, length($1)-1)}' > tmp.txt
sort -u tmp.txt > tmp2.txt
awk -i inplace '!seen[$0]++' tmp2.txt
cat tmp2.txt
echo "}"

echo "namespace exit_funcs {"
awk -F'exit_funcs::' '{if (length($2) > 0) print $2}' $1 | awk -F')' '{printf "    EX_RAPPLIC(%s);\n",substr($1, 1, length($1)-1)}' > tmp.txt
# Add a few additonal ones that are defined elsewhere, or for rooms that have multiple functions on a line.
echo "    EX_RAPPLIC(bkleavee);" >> tmp.txt
echo "    EX_RAPPLIC(carousel_out);" >> tmp.txt
echo "    EX_RAPPLIC(magnet_room_exit);" >> tmp.txt
echo "    EX_RAPPLIC(mrgo);" >> tmp.txt
echo "    EX_RAPPLIC(mirin);" >> tmp.txt
echo "    EX_RAPPLIC(mirout);" >> tmp.txt
echo "    EX_RAPPLIC(maybe_door);" >> tmp.txt
sort -u tmp.txt > tmp2.txt
awk -i inplace '!seen[$0]++' tmp2.txt
cat tmp2.txt
echo "}"

rm tmp.txt
rm tmp2.txt