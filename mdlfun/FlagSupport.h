#pragma once
#include <boost/serialization/serialization.hpp>

/// <summary>
/// This class mainly exists to allow std::bitset to be used
/// with scoped enums. A std::bitset doesn't allow a scoped enum
/// (like bs[FlagId::rainbow]). Not all features are implemented, but
/// just those that were required by other Zork code.
/// </summary>
/// <typeparam name="FlagType">Enum type to use.</typeparam>
/// <typeparam name="sz">Number of bits in the bitset.</typeparam>
template <typename FlagType, size_t sz>
class Flags : private std::bitset<sz>
{
    using Base = std::bitset<sz>;
public:
    Flags(const Base& src) : Base(src) 
    {}
    Flags() 
    {}

    using Base::none;
    using Base::any;
    using reference = Base::reference;

    bool operator[](FlagType flag) const { return Base::operator[](index(flag)); }
    reference operator[](FlagType flag)
    { 
        return Base::operator[](index(flag));
    }
    bool test(FlagType flag) const { return Base::test(index(flag)); }
    Flags& set() { Base::set(); return *this; }

    Flags& flip(FlagType flag)
    {
        Base::flip(index(flag));
        return *this;
    }

    friend class boost::serialization::access;
    template <class archive>
    void serialize(archive& ar, const unsigned int version)
    {
        Base& r = *this;
        ar & r;
    }

private:
    static typename std::underlying_type<FlagType>::type index(FlagType v)
    {
        return static_cast<typename std::underlying_type<FlagType>::type>(v);
    }

    template <typename FT, size_t Sz>
    friend Flags<FT, Sz> operator&(const Flags<FT, Sz>& lhs, const Flags<FT, Sz>& rhs);
};

template <typename FlagType, size_t sz>
Flags<FlagType, sz> operator&(const Flags<FlagType, sz>& lhs, const Flags<FlagType, sz>& rhs)
{
    using CBase = const Flags<FlagType, sz>::Base;
    Flags<FlagType, sz> rv(static_cast<CBase&>(lhs) & static_cast<CBase&>(rhs));
    return rv;
}
