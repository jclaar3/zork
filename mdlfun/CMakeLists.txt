﻿# CMakeList.txt : CMake project for zork_cmake, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.15)
cmake_policy(SET CMP0074 NEW)
cmake_policy(SET CMP0079 NEW)

if (MSVC)
	add_compile_definitions(_WIN32_WINNT=0x0601)
    add_compile_options("/Zi")
    add_link_options("/DEBUG")
endif()

if (WIN32)
    set(Boost_USE_STATIC_LIBS ON)
    set(Boost_USE_STATIC_RUNTIME ON)
endif()

add_compile_definitions(__STDC_WANT_LIB_EXT1__=1)

find_package(Boost 1.76.0 REQUIRED COMPONENTS filesystem serialization system)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

set(CMAKE_CXX_STANDARD 20)
set_source_files_properties(gobject.h objdefs.h objfns.h roomdefs.h roomfns.h zstring.h
    PROPERTIES GENERATED TRUE)

# Make sure we have at least gcc-13.
if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS "13")
        message(FATAL_ERROR "Must use at least gcc-13.")
    endif()
endif()

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

if (MSVC)
set(CMAKE_CXX_FLAGS_RELEASE "/O1 /Ob2 /DNDEBUG")
endif()

if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
add_compile_options("-Wno-parentheses")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -g -Xclang -gcodeview")
endif()

# Add source to this project's executable.
add_executable (zork
    act1.cpp act1.h
    act2.cpp act2.h
    act3.cpp act3.h
    act4.cpp act4.h
    adv.cpp adv.h
    cevent.cpp cevent.h
    defs.cpp defs.h
    dung.cpp dung.h
    FlagSupport.h
    funcs.cpp funcs.h
    globals.cpp globals.h
    gobject.h
    info.cpp info.h 
    makstr.cpp makstr.h 
    mdlfun.cpp
    melee.cpp melee.h 
    memq.cpp memq.h 
    objdefs.h
    object.cpp object.h 
    objfns.h 
    objser.h 
    parser.cpp parser.h
    room.cpp room.h 
    roomdefs.h 
    roomfns.cpp roomfns.h 
    rooms.cpp rooms.h 
    sr.cpp sr.h 
    stdafx.h 
    "strings.cpp" "strings.h"
    util.cpp util.h 
    ZorkException.h 
    zstring.h
     "version.h")
target_link_libraries(zork ${Boost_LIBRARIES})
#set_property(TARGET zork PROPERTY
#    MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
