#pragma once
#include <string>
#include <algorithm>
#include <iostream>

void proc_strings(const std::string &mdl, bool headers_only, std::ostream &os);
void proc_room(const std::string &mdl, std::ostream &os);
size_t passws(size_t start, const std::string &s);
size_t skiptows(size_t start, const std::string &s);
char convert(char c);
size_t dump_contents(size_t p, const std::string &mdl, std::ostream &os);
size_t dump_func(size_t p, const std::string &mdl, const std::string &nspac, std::ostream &ose);
size_t add_bits(size_t p, const std::string &mdl, std::ostream &os, const std::string &hdr);
size_t dump_props(size_t p, const std::string &mdl, const std::string &hdr, std::ostream &os);
size_t dump_string(size_t pos, const std::string &mdl, std::ostream &os, const std::string &tail = "");


inline void reduce(std::string &s)
{
    std::transform(s.begin(), s.end(), s.begin(), convert);
}
